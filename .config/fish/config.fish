starship init fish | source
zoxide init fish | source

# #########################
#       Vim Bindings
# #########################
fish_vi_key_bindings

# #########################
# Exa aliases
# #########################
alias ls='exa'

# list, size, type, git
alias l='exa -lbF --git'
# long list
alias ll='exa -lbGF --git'
# long list, modified date sort
alias llm='exa -lbGF --git --sort=modified'
# all list
alias lx='exa -lbhHigUmuSa@ --time-style=long-iso --git --color-scale'
# all + extended list
alias la='exa -lbhHigUmuSa --time-style=long-iso --git --color-scale'

## speciality views
# one column, just names
alias lS='exa -1'
# tree
alias lt='exa --tree --level=2'

# #########################
# cat aliases
# #########################

alias cat='bat'

# #########################
# Misc aliases
# #########################
alias xsh='TERM=xterm-256color ssh'
alias rot13='tr A-Za-z N-ZA-Mn-za-m'

# #########################
# variables
# #########################
set -xU NPM_TOKEN "bEy_VFCcbb4iF64x88hn"
set -xU CI_JOB_TOKEN $NPM_TOKEN
set -xU EDITOR nvim

# #########################
# Additions to PATH
# #########################
fish_add_path $HOME/.cargo/bin
fish_add_path $HOME/.local/bin
fish_add_path $HOME/1password-toolchain-macos/bin

# #########################
# Import os specific
# #########################
if test -f macos.fish
    source macos.fish
end

