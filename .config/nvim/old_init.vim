" Generic settings

:set tabstop=4
:set shiftwidth=4
:set softtabstop=4
:set expandtab
:set autoindent
:set mouse=a

" Install plugins
call plug#begin('~/.config/nvim/plugins')
 " Vim multi cursor support
 Plug 'mg979/vim-visual-multi', {'branch': 'master'}

 " Surround 
 Plug 'tpope/vim-surround'

 " For commenting gcc & gc
 Plug 'tpope/vim-commentary'

 " Tagbar for code navigation
 Plug 'preservim/tagbar'

 " I heard you like terminals so heres a terminal inside your terminal editor
 Plug 'tc50cal/vim-terminal'

 " LSP
 Plug 'neoclide/coc.nvim', {'branch': 'release'}

 " Airline
 Plug 'vim-airline/vim-airline'

 " Nerd tree
 Plug 'preservim/nerdtree'

 " Color schemes
 Plug 'rafi/awesome-vim-colorschemes'

 " Developer Icons
 Plug 'ryanoasis/vim-devicons'

call plug#end()

:colorscheme sonokai

" Set relative number when in normal mode, the normal line when in insert
set number relativenumber

augroup numbertoggle
        autocmd!
	autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
        autocmd BufLeave,FocusLost,InsertEnter *set norelativenumber
augroup END

""""""""""""""""
" key bindings "
""""""""""""""""
nnoremap <C-f> :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>

nnoremap <F8> :TagbarToggle<CR>

""""""""""""""""
" LSP settings "
""""""""""""""""
" use <tab> for trigger completion and navigate to the next complete item
function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
